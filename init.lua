minetest.register_craft({
	type = "fuel",
	recipe = "default:diamond",
	burntime = 60,
})

minetest.register_craft({
	type = "fuel",
	recipe = "default:diamondblock",
	burntime = 600,
})

minetest.register_craft({
	type = "fuel",
	recipe = "default:pick_diamond",
	burntime = 180,
})

minetest.register_craft({
	type = "fuel",
	recipe = "default:axe_diamond",
	burntime = 180,
})

minetest.register_craft({
	type = "fuel",
	recipe = "default:shovel_diamond",
	burntime = 60,
})

minetest.register_craft({
	type = "fuel",
	recipe = "default:sword_diamond",
	burntime = 120,
})

if minetest.get_modpath("fire") ~= nil then

	minetest.override_item("default:diamondblock", {
		on_ignite = function(pos, igniter)
			local pos2 = {x = pos.x, y = pos.y + 1, z = pos.z}
			if minetest.get_node(pos2).name == "air" then
				minetest.set_node(pos2, {name = "fire:permanent_flame"})
			end
		end,
		after_destruct = function(pos, node)
			pos.y = pos.y + 1
			if minetest.get_node(pos).name == "fire:permanent_flame" then
				minetest.remove_node(pos)
			end
		end,
	})

end
